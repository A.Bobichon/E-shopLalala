<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;

class GestionController extends Controller
{
    /**
     * @Route("/gestion", name="gestion")
     */
    public function index(ProductRepository $repository)
    {
        
        $products = $repository->findAll();

        return $this->render('gestion/index.html.twig', [
            'controller_name' => 'GestionController',
            'products' => $products
        ]);
    }
}
