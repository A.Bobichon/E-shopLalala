<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="array")
     */
    private $products;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $total;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LineProductCart", mappedBy="shoppingCartId")
     */
    private $lineProductCarts;

    public function __construct()
    {
        $this->lineProductCarts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getProducts(): ?array
    {
        return $this->products;
    }

    public function setProducts(array $products): self
    {
        $this->products = $products;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(?int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return Collection|LineProductCart[]
     */
    public function getLineProductCarts(): Collection
    {
        return $this->lineProductCarts;
    }

    public function addLineProductCart(LineProductCart $lineProductCart): self
    {
        if (!$this->lineProductCarts->contains($lineProductCart)) {
            $this->lineProductCarts[] = $lineProductCart;
            $lineProductCart->setShoppingCartId($this);
        }

        return $this;
    }

    public function removeLineProductCart(LineProductCart $lineProductCart): self
    {
        if ($this->lineProductCarts->contains($lineProductCart)) {
            $this->lineProductCarts->removeElement($lineProductCart);
            // set the owning side to null (unless already changed)
            if ($lineProductCart->getShoppingCartId() === $this) {
                $lineProductCart->setShoppingCartId(null);
            }
        }

        return $this;
    }
}
