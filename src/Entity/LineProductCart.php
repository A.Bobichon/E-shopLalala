<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LineProductCartRepository")
 */
class LineProductCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\product")
     */
    private $productId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShoppingCart", inversedBy="lineProductCarts")
     */
    private $shoppingCartId;

    public function getId()
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getProductId(): ?product
    {
        return $this->productId;
    }

    public function setProductId(?product $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function getShoppingCartId(): ?ShoppingCart
    {
        return $this->shoppingCartId;
    }

    public function setShoppingCartId(?ShoppingCart $shoppingCartId): self
    {
        $this->shoppingCartId = $shoppingCartId;

        return $this;
    }
}
