<?php

namespace App\Repository;

use App\Entity\LineProductCart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LineProductCart|null find($id, $lockMode = null, $lockVersion = null)
 * @method LineProductCart|null findOneBy(array $criteria, array $orderBy = null)
 * @method LineProductCart[]    findAll()
 * @method LineProductCart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LineProductCartRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LineProductCart::class);
    }

//    /**
//     * @return LineProductCart[] Returns an array of LineProductCart objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LineProductCart
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
